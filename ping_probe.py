#!/usr/bin/env python3

import argparse
import platform
import asyncio
import aiohttp
import sys
import os
import datetime
import json
import ipaddress
from subprocess import check_output
from tempfile import NamedTemporaryFile

MTR_BIN='./mtr'
MTR_ARGS=['njc5']
MTR_JSON_PATH=('report', 'hubs')
MTR_COUNT='count'
ERR_COUNT=99
MTR_MAX_RUNNER=20
_time_str = '%Y%m%d%H%M%S'
GL_HOP, GL_TARGET, GL_FINISH, GL_PROBER, GL_TDESC = (
    'hop', 'target', 'finish', 'host', 'target_desc')
GL_REPLACE_STR = {'Loss%': 'Loss', 'host': GL_HOP}
GL_SEQ=datetime.datetime.now().strftime(_time_str)
GL_KEY_FIELDS = {'message': GL_SEQ}


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'targets', 
        nargs='+', 
        help='Input one or more IP address.'
        'Also supports "IP,DESCRIPETION,HOP_IP1,HOP_NET/23,..." format')
    parser.add_argument(
        '-b',
        '--mtr_bin',
        default=MTR_BIN,
        help='Mtr binary file location, '
        'default location is {}'.format(MTR_BIN))
    parser.add_argument(
        '-i',
        '--id',
        default=get_prober_id(),
        help='IP_ADDR-LOGIN_USER')
    parser.add_argument(
        '-m',
        '--mtr_runner',
        default=MTR_MAX_RUNNER,
        help='By default, it will triger up to '
        '{} mtr processes'.format(MTR_MAX_RUNNER))
    parser.add_argument(
        '-u',
        '--url',
        default='',
        help='graylog server url, '
        'default gelf path http://YOUR.GRAYLOG.COM:PORT/gelf')
    parser.add_argument(
        '-p',
        '--mtr_args',
        action='append',
        default=[],
        help='Default setting is {}'.format(str(MTR_ARGS)))
    parser.add_argument(
        '--debug',
        default=False,
        action='store_true',
        help='Enable debug mode')
    p = parser.parse_args()
    if not p.mtr_args:
        p.mtr_args = MTR_ARGS
    p.mtr_args = ['-' + item for item in p.mtr_args]
    p.targets = [target_info(t) for t in p.targets]
    return p

def get_prober_id(out_template='{}_{}'):
    f = lambda x: check_output(x).decode().strip().split(' ')[0]
    ip = f(['hostname', '--all-ip-addresses'])
    user = f(['users'])
    return out_template.format(ip, user)

def target_info(t_string=None, _sep=',', desc=''):
    t_list = t_string.split(_sep)
    _len = len(t_list)
    if _len > 2:
        t_list = t_list[:2] + [t_list[2:]]
    else:
        # only have one element
        if _len < 2:
            t_list.append(desc)
        t_list.append([])
    return t_list

def str_to_ip(ip_str=None, converter=None):
    r = None
    try:
        r = converter(ip_str)
    except ValueError:
        pass
    return r

def data_formation(
        data=None, 
        replace_dict=GL_REPLACE_STR, 
        patch_dict=None,
        valid_hops=None):
    d = data[:]
    _valid_hops = []
    if valid_hops:
        _valid_hops = [str_to_ip(h, converter=ipaddress.ip_network)
                       for h in valid_hops]
    for k in replace_dict.keys():
        d = d.replace(k, replace_dict[k])
    d = json.loads(d)
    for node in MTR_JSON_PATH:
        d = d[node]
    #d = d[MTR_JSON_PATH[0]][MTR_JSON_PATH[1]]
    for item in d:
        item.update(patch_dict)
        # Force MTR COUNT to number
        try:
            item[MTR_COUNT] = int(item[MTR_COUNT])
        except ValueError:
            item[MTR_COUNT] = ERR_COUNT
        # Define what kind hop in the path
        hop = item[GL_HOP]
        if hop == patch_dict[GL_TARGET]:
            item[GL_FINISH] = 'yes'
        else:
            _hop = str_to_ip(hop, converter=ipaddress.ip_address)
            if _valid_hops and _hop:
                for vh in _valid_hops:
                    if _hop in vh:
                        item[GL_FINISH] = 'valid'
    return d

async def update(url='', data=None):
    async with aiohttp.ClientSession() as session:
        for d in data:
            async with session.post(url, json=d) as r:
                pass
                #print(r.status)

async def run_command(url=None, cli=None, pid=None, target=None, fstatus='no'):
    # Create subprocess
    process = await asyncio.create_subprocess_exec(
        *cli, 
        stdout=asyncio.subprocess.PIPE, 
        stderr=asyncio.subprocess.PIPE)
    print('Started:', cli, '(pid = ' + str(process.pid) + ')')
    stdout, stderr = await process.communicate()
    result = stdout.decode().strip()
    if process.returncode == 0:
        print('Done:', cli, '(pid = ' + str(process.pid) + ')')
        pdict = {
            **GL_KEY_FIELDS,
            **{GL_TARGET: cli[-1], 
               GL_FINISH: fstatus,
               GL_PROBER: pid},
               GL_TDESC: target[1]}
        try:
            r = data_formation(
                    result,
                    patch_dict=pdict,
                    valid_hops=target[-1])
        except json.JSONDecodeError:
            return result
        await update(url, r)
    else:
        print('Failed:', cli, '(pid = ' + str(process.pid) + ')')
    return result

def make_chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i + n]

def run_asyncio_commands(tasks, max_concurrent=0):
    all_results = []
    # Use chunks to create multiple batches task
    if max_concurrent == 0:
        chunks = [tasks]
    else:
        chunks = make_chunks(l=tasks, n=max_concurrent)
    for tasks_in_chunk in chunks:
        if platform.system() == 'Windows':
            loop = asyncio.ProactorEventLoop()
            asyncio.set_event_loop(loop)
        else:
            loop = asyncio.get_event_loop()
        commands = asyncio.gather(*tasks_in_chunk)
        # Run the commands from here
        results = loop.run_until_complete(commands)
        all_results += results
    loop.close()
    return all_results

def debug_dump(data=None):
    with NamedTemporaryFile(mode='w+',  delete=False) as f:
        print('Debug file path: {}'.format(f.name))
        f.writelines(data)

if __name__ == '__main__':
    args = get_args()
    tasks = [run_command(url=args.url, 
                         cli=(args.mtr_bin, *args.mtr_args, t[0]), 
                         pid=args.id, 
                         target=t) 
            for t in args.targets]
    results = run_asyncio_commands(
            tasks,
            max_concurrent=args.mtr_runner)
    if args.debug:
        debug_dump(results)
