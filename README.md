# README #

Opensource Ping Plot. An open source solution to visualize mtr record.

### What is this repository for? ###

* Create an open source solution to visulized mtr record 
* v0.00


### How do I get set up? ###
 
* Configuration:
     - ```$ pip3 install aiohttp```
     - ```$ sudo chown root:root mtr-packet && sudo chmod 4775 mtr-packet```
* Dependencies: graylog v3.0, grafana
* Mini help


```
usage: ping_probe.py [-h] [-b MTR_BIN] [-i ID] [-m MTR_RUNNER] [-u URL]
                     [-p MTR_ARGS] [--debug]
                     targets [targets ...]

positional arguments:
  targets               Input one or more IP address.Also supports
                        "IP,DESCRIPETION,HOP_IP1,HOP_NET/23,..." format

optional arguments:
  -h, --help            show this help message and exit
  -b MTR_BIN, --mtr_bin MTR_BIN
                        Mtr binary file location, default location is ./mtr
  -i ID, --id ID        IP_ADDR-LOGIN_USER
  -m MTR_RUNNER, --mtr_runner MTR_RUNNER
                        By default, it will triger up to 20 mtr processes
  -u URL, --url URL     graylog server url, default gelf path
                        http://YOUR.GRAYLOG.COM:PORT/gelf
  -p MTR_ARGS, --mtr_args MTR_ARGS
                        Default setting is ['njc5']
  --debug               Enable debug mode
```


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact